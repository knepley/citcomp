#!/usr/bin/env python
import os, sys
import numpy
import binascii

# print int(binascii.b2a_hex(bytes), 16)

def readHeader(basename):
  filename = basename+'.hdr'
  with open(filename, 'r') as f:
    solver = f.readline().strip()
    if not solver == 'multigrid':
      raise Exception('Not a multigrid run')
    mgX, mgY, mgZ, levels = map(int, f.readline().strip().split(' '))
    procX, procY, procZ = map(int, f.readline().strip().split(' '))
  return mgX * 2**(levels-1) + 1, mgY * 2**(levels-1) + 1, mgZ * 2**(levels-1) + 1, procX, procY, procZ

def readCitcomField(basename, field):
  numX, numY, numZ, procX, procY, procZ = readHeader(basename)
  # Global array
  array = numpy.zeros((numX, numY, numZ), dtype=numpy.float32)
  # Split into pieces
  numXsub = (numX + procX-1)/procX
  numYsub = (numY + procY-1)/procY
  numZsub = (numZ + procZ-1)/procZ
  #print numXsub, numYsub, numZsub
  for pZ in range(procZ):
    for pY in range(procY):
      for pX in range(procX):
        rank = (pY*procX + pX)*procZ + pZ
        #filename = '%s.%s.%d' % (basename, field, (pZ*procY + pY)*procX + pX)
        filename = '%s.%s.%d' % (basename, field, rank)
        if not os.path.exists(filename): filename += '.0'
        print('Reading '+filename)
        with open(filename, 'rb') as f:
          bytes = f.read(4) # Junk float
          shape  = (numYsub, numXsub, numZsub)
          subarray = numpy.fromfile(file=f, dtype=numpy.float32).reshape(shape)
          subarray = numpy.transpose(subarray, (1, 0, 2))
        #print 'X subslice',subarray[:,0,0]
        #print 'Y subslice',subarray[0,:,0]
        #print 'Z subslice',subarray[0,0,:]
        sX = ((numX-1)/procX)*pX; eX = ((numX-1)/procX)*(pX+1)+1
        sY = ((numY-1)/procY)*pY; eY = ((numY-1)/procY)*(pY+1)+1
        sZ = ((numZ-1)/procZ)*pZ; eZ = ((numZ-1)/procZ)*(pZ+1)+1
        print '  rank',rank,(sX,eX),(sY,eY),(sZ,eZ)
        array[sX:eX,sY:eY,sZ:eZ] = subarray
  #array = numpy.transpose(array, (1, 0, 2))
  return array

def readPetscFieldReorder(basename, field):
  numX, numY, numZ, procX, procY, procZ = readHeader(basename)
  filename = '%s.%s' % (basename, field)
  if not os.path.exists(filename): filename += '.0'
  print('Reading '+filename)
  print '  procs',procX, procY, procZ
  with open(filename, 'rb') as f:
    magicInt = int(binascii.b2a_hex(f.read(4)), 16)
    size     = int(binascii.b2a_hex(f.read(4)), 16)
    assert(size == numX*numY*numZ)
    parray = numpy.fromfile(file=f, dtype='>f8')
  # Global array
  array = numpy.zeros((numY, numX, numZ), dtype=numpy.float32)
  # Split into pieces
  offset  = 0
  sY = 0
  for pY in range(procY):
    sX = 0
    for pX in range(procX):
      sZ = 0
      for pZ in range(procZ):
        print '  rank',(pY*procX + pX)*procZ + pZ,'size',size,'offset',offset
        numXsub = numX/procX + (pX < numX%procX)
        numYsub = numY/procY + (pY < numY%procY)
        numZsub = numZ/procZ + (pZ < numZ%procZ)
        print '  patch size',(numXsub,numYsub,numZsub)
        eX = sX+numXsub
        eY = sY+numYsub
        eZ = sZ+numZsub
        print '  patch',(sX,eX),(sY,eY),(sZ,eZ)
        sizeSub = numXsub*numYsub*numZsub
        print '  raw',parray[offset:offset+sizeSub].shape,sizeSub
        newparray = parray[offset:offset+sizeSub].reshape((numYsub,numXsub,numZsub))
        print '  shapes',array[sY:eY,sX:eX,sZ:eZ].shape,newparray.shape
        array[sY:eY,sX:eX,sZ:eZ] = newparray
        offset += sizeSub
        sZ = eZ
      sX = eX
    sY = eY
  assert(offset == numX*numY*numZ)
  newarray = numpy.transpose(array, (1, 0, 2))
  #print 'parray',array
  return newarray

def readPetscField(basename, field):
  numX, numY, numZ, procX, procY, procZ = readHeader(basename)
  filename = '%s.%s' % (basename, field)
  if not os.path.exists(filename): filename += '.0'
  print('Reading '+filename)
  with open(filename, 'rb') as f:
    magicInt = int(binascii.b2a_hex(f.read(4)), 16)
    size     = int(binascii.b2a_hex(f.read(4)), 16)
    shape  = (numY, numX, numZ)
    print shape
    #array = numpy.fromfile(file=f, dtype='>f8').reshape(shape)
    array = numpy.fromfile(file=f, dtype='>f8')
    print array
    array = array.reshape(shape)
    array = numpy.transpose(array, (2, 1, 0))
    #array = numpy.transpose(array, (1, 0, 2))
    #print 'X slice',array[:,0,0]
    #print 'Y slice',array[0,:,0]
    #print 'Z slice',array[0,0,:]
  return array

def readField(basename, format, field):
  if format == 'C':
    return readCitcomField(basename, field)
  elif format == 'P':
    return readPetscFieldReorder(basename, field)
  raise Exception('Invalid field format '+str(format))

if __name__ == '__main__':
  if len(sys.argv) == 4:
    # Viewing
    basename = sys.argv[1]
    format   = sys.argv[2]
    field    = sys.argv[3]
    array    = readField(basename, format, field)
    print 'X slice',array[:,0,0]
    print 'Y slice',array[0,:,0]
    print 'Z slice',array[0,0,:]
    print array
  elif len(sys.argv) == 6:
    # Comparison
    basenameA = sys.argv[1]
    formatA   = sys.argv[2]
    basenameB = sys.argv[3]
    formatB   = sys.argv[4]
    field     = sys.argv[5]
    arrayA    = readField(basenameA, formatA, field)
    arrayB    = readField(basenameB, formatB, field)
    isEqual   = numpy.max(arrayA - arrayB) < 1.0e-10
    if not isEqual:
      print 'Fields are NOT Equal'
      y = z = 0
      print 'X slice for A',arrayA[:,y,z]
      print 'X slice for B',arrayB[:,y,z]
      print numpy.max(abs(arrayA[:,y,z] - arrayB[:,y,z])) < 1.0e-10
      print 'Y slice for A',arrayA[0,:,0]
      print 'Y slice for B',arrayB[0,:,0]
      print numpy.max(abs(arrayA[0,:,0] - arrayB[0,:,0])) < 1.0e-10
      print 'Z slice for A',arrayA[0,0,:]
      print 'Z slice for B',arrayB[0,0,:]
      print numpy.max(abs(arrayA[0,0,:] - arrayB[0,0,:])) < 1.0e-10
      for y in range(13):
        for z in range(13):
          if not numpy.max(abs(arrayA[:,y,z] - arrayB[:,y,z])) < 1.0e-10:
            print 'y',y,'z',z
            print 'X slice for A',arrayA[:,y,z]
            print 'X slice for B',arrayB[:,y,z]
            print 'X slice diff',arrayA[:,y,z] - arrayB[:,y,z]
      #print arrayA
      #print arrayB
      sys.exit(1)
    else:
      print 'Fields are Equal'
  else:
    print 'Invalid argument list'
    sys.exit(2)
