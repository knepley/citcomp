/*
 * CitcomCU is a Finite Element Code that solves for thermochemical
 * convection within a three dimensional domain appropriate for convection
 * within the Earth's mantle. Cartesian and regional-spherical geometries
 * are implemented. See the file README contained with this distribution
 * for further details.
 * 
 * Copyright (C) 1994-2005 California Institute of Technology
 * Copyright (C) 2000-2005 The University of Colorado
 *
 * Authors: Louis Moresi, Shijie Zhong, and Michael Gurnis
 *
 * For questions or comments regarding this software, you may contact
 *
 *     Luis Armendariz <luis@geodynamics.org>
 *     http://geodynamics.org
 *     Computational Infrastructure for Geodynamics (CIG)
 *     California Institute of Technology
 *     2750 East Washington Blvd, Suite 210
 *     Pasadena, CA 91007
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 2 of the License, or any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/*   Functions which solve for the velocity and pressure fields using Uzawa-type iteration loop.  */

#include <petsc.h>
#include "element_definitions.h"
#include "global_defs.h"

extern int Emergency_stop;

#undef __FUNCT__
#define __FUNCT__ "CitcomResidual"
/* 
     res_u = -div (nu grad u) - grad p - f
     res_p = div u

     assemble_grad_p(E, P, gradP, lev)
     assemble_del2_u(E, U, del2U, lev, 1)
     assemble_div_u(E, U, divU, lev)
     res_u = F - del2U - gradP
     strip_bcs_from_residual(E, res_u, lev)
     res_p = divU
*/
PetscErrorCode CitcomResidual(struct All_variables *E)
{
  DM             dmF = E->lmesh.levelFullDA[E->mesh.levmax];
  DM             dmV = E->lmesh.levelVeloDA[E->mesh.levmax];
  DM             dmP = E->lmesh.levelPresDA[E->mesh.levmax];
  VecScatter     scV = E->lmesh.veloMap;
  VecScatter     scP = E->lmesh.presMap;
  PetscInt       nP  = E->mesh.npno;
  PetscInt       nV  = E->mesh.neq;
  Vec            resUVec, gradPVec, del2UVec, divUVec, gU, gP, gR;
  PetscScalar   *resU, *gradP, *del2U, *divU;
  PetscReal      rU, rP, rF, normU;
  PetscReal      n1, n2, n3;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMGetLocalVector(dmV, &resUVec);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dmV, &gradPVec);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dmV, &del2UVec);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dmP, &divUVec);CHKERRQ(ierr);
  ierr = VecGetArray(gradPVec, &gradP);CHKERRQ(ierr); --gradP;
  ierr = VecGetArray(del2UVec, &del2U);CHKERRQ(ierr); --del2U;
  ierr = VecGetArray(divUVec,  &divU);CHKERRQ(ierr);  --divU;
  ierr = VecGetArray(E->UVec,  &E->U);CHKERRQ(ierr);  --E->U;
  ierr = VecGetArray(E->PVec,  &E->P);CHKERRQ(ierr);  --E->P;
  assemble_grad_p(E, E->P, gradP, E->mesh.levmax);
  assemble_del2_u(E, E->U, del2U, E->mesh.levmax, 1);
  assemble_div_u(E, E->U, divU, E->mesh.levmax);
  ++E->U;  ierr = VecRestoreArray(E->UVec,  &E->U);CHKERRQ(ierr);
  ++E->P;  ierr = VecRestoreArray(E->PVec,  &E->P);CHKERRQ(ierr);
  ++divU;  ierr = VecRestoreArray(divUVec,  &divU);CHKERRQ(ierr);
  ++del2U; ierr = VecRestoreArray(del2UVec, &del2U);CHKERRQ(ierr);
  ++gradP; ierr = VecRestoreArray(gradPVec, &gradP);CHKERRQ(ierr);
  ierr = VecCopy(E->FVec, resUVec);CHKERRQ(ierr);

  ierr = VecNorm(del2UVec, NORM_2, &n1);CHKERRQ(ierr);
  ierr = VecNorm(gradPVec, NORM_2, &n2);CHKERRQ(ierr);
  ierr = VecNorm(E->FVec, NORM_2, &n3);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Local del2U: %g gradP: %g F: %g (%g)\n", n1, n2, n3, n3/sqrt(nV));CHKERRQ(ierr);

  ierr = VecAXPBYPCZ(resUVec, -1.0, -1.0, 1.0, del2UVec, gradPVec);CHKERRQ(ierr);
  ierr = VecGetArray(resUVec,  &resU);CHKERRQ(ierr);  --resU;
  strip_bcs_from_residual(E, resU, E->mesh.levmax);
  ++resU;  ierr = VecRestoreArray(resUVec,  &resU);CHKERRQ(ierr);

  ierr = VecNorm(resUVec, NORM_2, &rU);CHKERRQ(ierr);
  ierr = VecNorm(divUVec, NORM_2, &rP);CHKERRQ(ierr);
  ierr = VecNorm(E->UVec, NORM_2, &normU);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Local resU: %g (%g) resP: %g divU/U: %g (%g)\n", rU, rU/sqrt(nV), rP, rP/normU, (rP/sqrt(nP))/(normU/sqrt(nV)));CHKERRQ(ierr);

  ierr = DMGetGlobalVector(dmF, &gR);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dmV, &gU);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dmP, &gP);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(dmP, divUVec, INSERT_VALUES, gP);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dmP, divUVec, INSERT_VALUES, gP);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(dmV, resUVec, INSERT_VALUES, gU);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dmV, resUVec, INSERT_VALUES, gU);CHKERRQ(ierr);
  ierr = VecScatterBegin(scP, gP, gR, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd(scP, gP, gR, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterBegin(scV, gU, gR, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd(scV, gU, gR, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dmV, &resUVec);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dmV, &gradPVec);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dmV, &del2UVec);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dmP, &divUVec);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dmP, &gP);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dmV, &gU);CHKERRQ(ierr);
  ierr = VecNorm(gR, NORM_2, &rF);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Global res: %g\n", rF);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dmF, &gR);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CitcomLinearSolver"
PetscErrorCode CitcomLinearSolver(PC pc, Vec x, Vec y)
{
  struct All_variables *E;
  DM             dmV, dmP;
  VecScatter     scV, scP;
  Vec            gU, gP;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PCShellGetContext(pc, (void **) &E);CHKERRQ(ierr);
  dmV  = E->lmesh.levelVeloDA[E->mesh.levmax];
  dmP  = E->lmesh.levelPresDA[E->mesh.levmax];
  scV  = E->lmesh.veloMap;
  scP  = E->lmesh.presMap;
  ierr = DMGetGlobalVector(dmV, &gU);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dmP, &gP);CHKERRQ(ierr);
  /* Setup Input */
  ierr = VecScatterBegin(scV, x, gU, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(scV, x, gU, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dmV, gU, INSERT_VALUES, E->FVec);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmV, gU, INSERT_VALUES, E->FVec);CHKERRQ(ierr);
  /* Notice that we need E->UVec and E->PVec already setup for the initial guess */
  ierr = solve_constrained_flow_iterative(E);CHKERRQ(ierr);
  /* Check full residual */
  ierr = CitcomResidual(E);CHKERRQ(ierr);
  /* Read out answer */
  ierr = DMLocalToGlobalBegin(dmP, E->PVec, INSERT_VALUES, gP);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dmP, E->PVec, INSERT_VALUES, gP);CHKERRQ(ierr);
  ierr = VecScatterBegin(scP, gP, y, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd(scP, gP, y, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(dmV, E->UVec, INSERT_VALUES, gU);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dmV, E->UVec, INSERT_VALUES, gU);CHKERRQ(ierr);
  ierr = VecScatterBegin(scV, gU, y, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd(scV, gU, y, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dmV, &gU);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dmP, &gP);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* Master loop for pressure and (hence) velocity field */
#undef __FUNCT__
#define __FUNCT__ "solve_constrained_flow_iterative"
PetscErrorCode solve_constrained_flow_iterative(struct All_variables *E)
{
	double         residual_ddash;
	int            cycles = E->control.p_iterations;
    PetscErrorCode ierr;

    PetscFunctionBeginUser;
    ierr = VecGetArray(E->UVec, &E->U);CHKERRQ(ierr); --E->U;
    ierr = VecGetArray(E->FVec, &E->F);CHKERRQ(ierr); --E->F;
    ierr = VecGetArray(E->PVec, &E->P);CHKERRQ(ierr); --E->P;
	/* Solve for velocity and pressure, correct for bc's */
	ierr = solve_Ahat_p_fhat(E, E->U, E->P, E->F, E->control.accuracy, &cycles, &residual_ddash);CHKERRQ(ierr);
    ++E->F; ierr = VecRestoreArray(E->FVec, &E->F);CHKERRQ(ierr);
    ++E->P; ierr = VecRestoreArray(E->PVec, &E->P);CHKERRQ(ierr);

	v_from_vector(E, E->V, E->U);
	/* p_to_nodes(E,E->P,E->NP,E->mesh.levmax); */  
    ++E->U; ierr = VecRestoreArray(E->UVec, &E->U);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}



/*  ==========================================================================  */
#undef __FUNCT__
#define __FUNCT__ "solve_Ahat_p_fhat"
PetscErrorCode solve_Ahat_p_fhat(struct All_variables *E, double *V, double *P, double *F, double imp, int *steps_max, double *pressureResidual)
{
	//int i, j, k, ii, count, convergent, valid, problems, lev, lev_low, npno, neq, steps;
	int i, j, count, convergent, valid, problems, lev, npno, neq;
	int gnpno, gneq;

	static int been_here = 0;
	//double *u;
	//static double *p1, *r1, *r0, *r2, *z0, *z1, *s1, *s2, *Ah, *u1;
	Vec         r1V, r0V, r2V, z0V, z1V, s1V, s2V, AhV, u1V;
	PetscScalar *r1, *r0, *r2, *z0, *z1, *s1, *s2, *Ah, *u1;
	//double *shuffle, *R;
	double *shuffle;
	double alpha, delta, s2dotAhat, r0dotr0 = 0.0, r1dotz1;
	//double residual, initial_residual, last_residual, res_magnitude, v_res;
	double residual, initial_residual, res_magnitude, v_res = 0.0, tmp, tmp2;

	double time0, time;
	static double timea;
	//float dpressure, dvelocity, tole_comp;
	float dpressure, dvelocity;
    PetscErrorCode ierr;

	//const int dims = E->mesh.nsd;
	//const int n = loc_mat_size[E->mesh.nsd];

    PetscFunctionBegin;
	npno = E->lmesh.npno;
	neq = E->lmesh.neq;

	gnpno = E->mesh.npno;
	gneq = E->mesh.neq;

    ierr = DMGetLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &r0V);CHKERRQ(ierr);
    ierr = DMGetLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &r1V);CHKERRQ(ierr);
    ierr = DMGetLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &r2V);CHKERRQ(ierr);
    ierr = DMGetLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &z0V);CHKERRQ(ierr);
    ierr = DMGetLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &z1V);CHKERRQ(ierr);
    ierr = DMGetLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &s1V);CHKERRQ(ierr);
    ierr = DMGetLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &s2V);CHKERRQ(ierr);
    ierr = DMGetLocalVector(E->lmesh.levelVeloDA[E->mesh.levmax], &AhV);CHKERRQ(ierr);
    ierr = DMGetLocalVector(E->lmesh.levelVeloDA[E->mesh.levmax], &u1V);CHKERRQ(ierr);
    {
      PetscInt size;
      ierr = VecGetSize(r0V, &size);CHKERRQ(ierr);
      if (size != npno) SETERRQ2(PETSC_COMM_SELF, PETSC_ERR_ARG_SIZ, "%d != %d", size, npno);
      ierr = VecGetSize(u1V, &size);CHKERRQ(ierr);
      if (size != neq) SETERRQ2(PETSC_COMM_SELF, PETSC_ERR_ARG_SIZ, "%d != %d", size, neq);
    }
    ierr = VecGetArray(r0V, &r0);CHKERRQ(ierr); --r0;
    ierr = VecGetArray(r1V, &r1);CHKERRQ(ierr); --r1;
    ierr = VecGetArray(r2V, &r2);CHKERRQ(ierr); --r2;
    ierr = VecGetArray(z0V, &z0);CHKERRQ(ierr); --z0;
    ierr = VecGetArray(z1V, &z1);CHKERRQ(ierr); --z1;
    ierr = VecGetArray(s1V, &s1);CHKERRQ(ierr); --s1;
    ierr = VecGetArray(s2V, &s2);CHKERRQ(ierr); --s2;
    ierr = VecGetArray(AhV, &Ah);CHKERRQ(ierr); --Ah;
    ierr = VecGetArray(u1V, &u1);CHKERRQ(ierr); --u1;
	if (been_here == 0) {
      been_here = 1;
      timea = CPU_time0();
	}

	problems = 0;
	time0 = time = CPU_time0();

	been_here++;

	/* calculate the velocity residual, note there are tricks involved here */

	lev = E->mesh.levmax;

    ierr = global_vdot(E, F, F, lev, &v_res);CHKERRQ(ierr);
	v_res = sqrt(v_res / gneq);

	if(E->parallel.me == 0) {fprintf(E->fp,"initial residual of momentum equation %g %d\n", v_res, gneq);}
    ierr = PetscPrintf(PETSC_COMM_WORLD, "initial residual of momentum equation %g %d\n", v_res, gneq);CHKERRQ(ierr);
	assemble_grad_p(E, P, Ah, lev);
	assemble_del2_u(E, V, u1, lev, 1);

	for(i = 0; i < neq; i++)
		Ah[i] = F[i] - Ah[i] - u1[i];

	strip_bcs_from_residual(E, Ah, lev);

	ierr = solve_del2_u(E, u1, Ah, imp * v_res, E->mesh.levmax, 0, &valid);CHKERRQ(ierr);
	strip_bcs_from_residual(E, u1, lev);

	for(i = 0; i < neq; i++)
		V[i] += u1[i];

	assemble_div_u(E, V, r1, lev);

    ierr = global_pdot(E, r1, r1, lev, &initial_residual);CHKERRQ(ierr);
	residual = initial_residual = sqrt(initial_residual / gnpno);

    ierr = global_vdot(E, V, V, lev, &tmp);CHKERRQ(ierr);
	E->monitor.vdotv = sqrt(tmp / gneq);

	E->monitor.incompressibility = residual / E->monitor.vdotv;

	if (E->control.print_convergence) {
      if (E->parallel.me == 0)  {
        fprintf(E->fp, "Loop to reduce pressure residual %g\n", residual);
        fflush(E->fp);
      }
      ierr = PetscPrintf(PETSC_COMM_WORLD, "Loop to reduce pressure residual %g\n", residual);CHKERRQ(ierr);
	}

	count = 0;
	convergent = 0;

	dpressure = 1.0;
	dvelocity = 1.0;

	res_magnitude = residual;

	if(E->control.print_convergence && E->parallel.me == 0)
	{
      fprintf(E->fp, "AhatP (%03d) after %g sec %g sec with div/v=%.3e for step %d\n", count, CPU_time0() - time0, CPU_time0() - timea, E->monitor.incompressibility, E->monitor.solution_cycles);
      if (E->monitor.time_output) {
        ierr = PetscPrintf(PETSC_COMM_WORLD, "AhatP (%03d) after %g sec %g sec with div/v=%.3e for step %d\n", count, CPU_time0() - time0, CPU_time0() - timea, E->monitor.incompressibility, E->monitor.solution_cycles);CHKERRQ(ierr);
      } else {
        ierr = PetscPrintf(PETSC_COMM_WORLD, "AhatP (%03d) with div/v=%.3e for step %d\n", count, E->monitor.incompressibility, E->monitor.solution_cycles);CHKERRQ(ierr);
      }
	}

/*   while( (count < *steps_max) && (E->monitor.incompressibility >= E->control.tole_comp || dvelocity >= imp) )  {     
*/ while((count < *steps_max) && (dpressure >= imp || dvelocity >= imp))
	{

		for(j = 1; j <= npno; j++)
			z1[j] = E->BPI[lev][j] * r1[j];

		ierr = global_pdot(E, r1, z1, lev, &r1dotz1);CHKERRQ(ierr);

		if((count == 0))
			for(j = 1; j <= npno; j++)
				s2[j] = z1[j];
		else
		{
            ierr = global_pdot(E, r0, z0, lev, &r0dotr0);CHKERRQ(ierr);
			assert(r0dotr0 != 0.0 /* Division by zero in head of incompressibility iteration */ );
			delta = r1dotz1 / r0dotr0;
			for(j = 1; j <= npno; j++)
				s2[j] = z1[j] + delta * s1[j];
		}

		assemble_grad_p(E, s2, Ah, lev);

		ierr = solve_del2_u(E, u1, Ah, imp * v_res, lev, 1, &valid);CHKERRQ(ierr);
		strip_bcs_from_residual(E, u1, lev);

		assemble_div_u(E, u1, Ah, lev);

		ierr = global_pdot(E, s2, Ah, lev, &s2dotAhat);CHKERRQ(ierr);

		if(valid)
			alpha = r1dotz1 / s2dotAhat;
		else
			alpha = 0.0;

		for(j = 1; j <= npno; j++)
		{
			r2[j] = r1[j] - alpha * Ah[j];
			P[j] += alpha * s2[j];
		}

		for(j = 0; j < neq; j++)
			V[j] -= alpha * u1[j];

		assemble_div_u(E, V, Ah, lev);
		ierr = global_vdot(E, V, V, E->mesh.levmax, &tmp);CHKERRQ(ierr);
        E->monitor.vdotv = tmp;
        ierr = global_pdot(E, Ah, Ah, lev, &tmp);CHKERRQ(ierr);
		E->monitor.incompressibility = sqrt((gneq / gnpno) * (1.0e-32 + tmp / (1.0e-32 + E->monitor.vdotv)));
        ierr = global_pdot(E, s2, s2, lev, &tmp);CHKERRQ(ierr);
        ierr = global_pdot(E, P, P, lev, &tmp2);CHKERRQ(ierr);
		dpressure = alpha * sqrt(tmp / (1.0e-32 + tmp2));
        ierr = global_vdot(E, u1, u1, lev, &tmp);CHKERRQ(ierr);
		dvelocity = alpha * sqrt(tmp / (1.0e-32 + E->monitor.vdotv));

		count++;
		if (E->control.print_convergence) {
          if (E->parallel.me == 0) {
            fprintf(E->fp, "AhatP (%03d) after %g sec with div/v=%.3e, dv/v=%.3e & dp/p=%.3e for step %d\n", count, CPU_time0() - time0, E->monitor.incompressibility, dvelocity, dpressure, E->monitor.solution_cycles);
            fflush(E->fp);
          }
          if (E->monitor.time_output) {
            ierr = PetscPrintf(PETSC_COMM_WORLD, "AhatP (%03d) after %g sec with div/v=%.3e, dv/v=%.3e & dp/p=%.3e for step %d\n", count, CPU_time0() - time0, E->monitor.incompressibility, dvelocity, dpressure, E->monitor.solution_cycles);CHKERRQ(ierr);
          } else {
            ierr = PetscPrintf(PETSC_COMM_WORLD, "AhatP (%03d) with div/v=%.3e, dv/v=%.3e & dp/p=%.3e for step %d\n", count, E->monitor.incompressibility, dvelocity, dpressure, E->monitor.solution_cycles);CHKERRQ(ierr);
          }
		}

		shuffle = s1;
		s1 = s2;
		s2 = shuffle;
		shuffle = r0;
		r0 = r1;
		r1 = r2;
		r2 = shuffle;
		shuffle = z0;
		z0 = z1;
		z1 = shuffle;

	}							/* end loop for conjugate gradient   */

	if(problems)
	{
		fprintf(E->fp, "Convergence of velocity solver may affect continuity\n");
		fprintf(E->fp, "Consider running with the `see_convergence=on' option\n");
		fprintf(E->fp, "To evaluate the performance of the current relaxation parameters\n");
		fflush(E->fp);
	}

	if (E->control.print_convergence) {
      if (E->parallel.me == 0) {fprintf(E->fp, "after (%03d) pressure loops and %g sec for step %d\n", count, CPU_time0() - timea, E->monitor.solution_cycles);}
		if (E->monitor.time_output) {
          ierr = PetscPrintf(PETSC_COMM_WORLD, "after (%03d) pressure loops and %g sec for step %d\n", count, CPU_time0() - timea, E->monitor.solution_cycles);CHKERRQ(ierr);
        } else {
          ierr = PetscPrintf(PETSC_COMM_WORLD, "after (%03d) pressure loops for step %d\n", count, E->monitor.solution_cycles);CHKERRQ(ierr);
        }
		if (count == *steps_max) { /* will be == b/c count is updated at end of while loop. FailSafe Added 9/19/2007 MAJ */
          if (E->parallel.me == 0) {
            fprintf(E->fp, "WARNING: count(%d) == steps_max(%d). dv/v = %.3e and dp/p = %.3e both should be <= %.3e :WARNING\n", count, *steps_max, dvelocity, dpressure, imp);
            fflush(E->fp);
          }
          ierr = PetscPrintf(PETSC_COMM_WORLD, "WARNING: count(%d) == steps_max(%d). dv/v = %.3e and dp/p = %.3e both should be <= %.3e :WARNING\n", count, *steps_max, dvelocity, dpressure, imp);CHKERRQ(ierr);
		}
	}

    CHKMEMQ;
    ++r0; ierr = VecRestoreArray(r0V, &r0);CHKERRQ(ierr);
    ++r1; ierr = VecRestoreArray(r1V, &r1);CHKERRQ(ierr);
    ++r2; ierr = VecRestoreArray(r2V, &r2);CHKERRQ(ierr);
    ++z0; ierr = VecRestoreArray(z0V, &z0);CHKERRQ(ierr);
    ++z1; ierr = VecRestoreArray(z1V, &z1);CHKERRQ(ierr);
    ++s1; ierr = VecRestoreArray(s1V, &s1);CHKERRQ(ierr);
    ++s2; ierr = VecRestoreArray(s2V, &s2);CHKERRQ(ierr);
    ++Ah; ierr = VecRestoreArray(AhV, &Ah);CHKERRQ(ierr);
    ++u1; ierr = VecRestoreArray(u1V, &u1);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &r0V);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &r1V);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &r2V);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &z0V);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &z1V);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &s1V);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(E->lmesh.levelPresDA[E->mesh.levmax], &s2V);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(E->lmesh.levelVeloDA[E->mesh.levmax], &AhV);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(E->lmesh.levelVeloDA[E->mesh.levmax], &u1V);CHKERRQ(ierr);

	*steps_max = count;
	*pressureResidual = residual;
    PetscFunctionReturn(0);
}


/*  ==========================================================================  */

void v_from_vector(struct All_variables *E, float **V, double *F)
{
	int node;
	//int node, d;
	//unsigned int type;

	const int nno = E->lmesh.nno;
	//const int dofs = E->mesh.dof;

	for(node = 1; node <= nno; node++)
	{
		V[1][node] = F[E->id[node].doff[1]];
		V[2][node] = F[E->id[node].doff[2]];
		V[3][node] = F[E->id[node].doff[3]];
		if(E->node[node] & VBX)
			V[1][node] = E->VB[1][node];
		if(E->node[node] & VBZ)
			V[3][node] = E->VB[3][node];
		if(E->node[node] & VBY)
			V[2][node] = E->VB[2][node];
	}
	return;
}
