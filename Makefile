#
#  Makefile for "CitcomP"
#
####################################################################
#	CitcomCU's files .....
####################################################################

EXECUTABLE = citcomcu
CFILES= Advection_diffusion.c\
	Boundary_conditions.c\
	Citcom.c\
	Composition_adv.c\
	Construct_arrays.c\
	Convection.c\
	Drive_solvers.c\
	Element_calculations.c\
	Geometry_cartesian.c\
	General_matrix_functions.c\
	Global_operations.c\
	Stokes_flow_Incomp.c\
	Instructions.c\
	Nodal_mesh.c\
	Output.c\
	Pan_problem_misc_functions.c\
	Parallel_related.c\
	Parsing.c\
	Phase_change.c\
	Process_buoyancy.c\
	Process_velocity.c\
	Profiling.c\
	Shape_functions.c\
	Size_does_matter.c\
	Solver_multigrid.c\
	Solver_conj_grad.c\
	Sphere_harmonics.c\
	Topo_gravity.c\
	Viscosity_structures.c

HEADERS = element_definitions.h\
	      global_defs.h\
	      viscosity_descriptions.h\
	      advection.h

OBJFILES = $(CFILES:.c=.o)


####################################################################
# Makefile rules follow
####################################################################

default: ${EXECUTABLE}

${EXECUTABLE}: ${OBJFILES} ${HEADERS} Makefile
	${CLINKER}  -o ${EXECUTABLE} ${OBJFILES} ${PETSC_LIB}
	${DSYMUTIL} ${EXECUTABLE}

clean::
	${RM} *.o

clean-all: clean
	${RM} ${EXECUTABLE}

##############################################################################
#	Tests
##############################################################################
check_serial: test_serial1_0 test_serial1_1
check_parallel: test_parallel1_0 test_parallel1_1 test_parallel2_0 test_parallel2_1 test_parallel3_0 test_parallel3_1
check_parallel_global: test_parallel1_0_global test_parallel1_0_petsc test_parallel2_0_global test_parallel2_0_petsc test_parallel3_0_global test_parallel3_0_petsc
check_fields: test_serial1_serial1 test_serial1_parallel1 test_serial1_parallel2 test_serial1_parallel3
check_petsc: test_serial1_0_petsc test_parallel1_0_petsc test_parallel2_0_petsc test_parallel3_0_petsc
check: check_serial check_parallel check_fields check_parallel_global

test_serial1_0:
	-${MPIEXEC} -n 1 ./citcomcu ./tests/serial1 -time_output 0 > serial1_0.tmp 2>&1; \
      ${DIFF} tests/output/serial1_0.out serial1_0.tmp || echo ${PWD} "\nPossible problem with with serial1_0, diffs above \n======================================"; \
      ${RM} serial1_0.tmp

test_serial1_0_valgrind:
	-valgrind --suppressions=./share/valgrind/suppressions ./citcomcu ./tests/serial1 -time_output 0 > serial1_0_valgrind.tmp 2>&1; \
      cat serial1_0_valgrind.tmp | grep ERROR | cut -c 11-62 > serial1_0_valgrind_2.tmp; \
      ${DIFF} tests/output/serial1_0_valgrind.out serial1_0_valgrind_2.tmp || echo ${PWD} "\nPossible problem with with serial1_0_valgrind, diffs above \n======================================"; \
      #${RM} serial1_0_valgrind.tmp serial1_0_valgrind_2.tmp

test_serial1_0_petsc:
	-${MPIEXEC} -n 1 ./citcomcu ./tests/serial1 -time_output 0 -use_petsc_linear_solver > serial1_0.tmp 2>&1; \
      ${DIFF} tests/output/serial1_0.out serial1_0.tmp || echo ${PWD} "\nPossible problem with with serial1_0, diffs above \n======================================"; \
      ${RM} serial1_0.tmp

test_serial1_1:
	-@${MPIEXEC} -n 1 ./citcomcu ./tests/serial1 -time_limit 1.0e-3 -time_output 0 > serial1_1.tmp 2>&1; \
      ${DIFF} tests/output/serial1_1.out serial1_1.tmp || echo ${PWD} "\nPossible problem with with serial1_1, diffs above \n======================================"; \
      ${RM} serial1_1.tmp

test_parallel1_0:
	-@${MPIEXEC} -n 4 ./citcomcu ./tests/parallel1 -time_output 0 > parallel1_0.tmp 2>&1; \
      ${DIFF} tests/output/parallel1_0.out parallel1_0.tmp || echo ${PWD} "\nPossible problem with with parallel1_0, diffs above \n======================================"; \
      ${RM} parallel1_0.tmp

test_parallel1_0_global:
	-@${MPIEXEC} -n 4 ./citcomcu ./tests/parallel1 -time_output 0 -use_global_solution > parallel1_0_global.tmp 2>&1; \
      ${DIFF} tests/output/parallel1_0_global.out parallel1_0_global.tmp || echo ${PWD} "\nPossible problem with with parallel1_0_global, diffs above \n======================================"; \
      ${RM} parallel1_0_global.tmp

test_parallel1_0_petsc:
	-@${MPIEXEC} -n 4 ./citcomcu ./tests/parallel1 -time_output 0 -use_petsc_linear_solver > parallel1_0_global.tmp 2>&1; \
      ${DIFF} tests/output/parallel1_0_global.out parallel1_0_global.tmp || echo ${PWD} "\nPossible problem with with parallel1_0_petsc, diffs above \n======================================"; \
      ${RM} parallel1_0_global.tmp

test_parallel1_1:
	-@${MPIEXEC} -n 4 ./citcomcu ./tests/parallel1 -time_limit 1.0e-3 -time_output 0 > parallel1_1.tmp 2>&1; \
      ${DIFF} tests/output/parallel1_1.out parallel1_1.tmp || echo ${PWD} "\nPossible problem with with parallel1_1, diffs above \n======================================"; \
      ${RM} parallel1_1.tmp

test_parallel2_0:
	-@${MPIEXEC} -n 4 ./citcomcu ./tests/parallel2 -time_output 0 > parallel2_0.tmp 2>&1; \
      ${DIFF} tests/output/parallel2_0.out parallel2_0.tmp || echo ${PWD} "\nPossible problem with with parallel2_0, diffs above \n======================================"; \
      ${RM} parallel2_0.tmp

test_parallel2_0_global:
	-@${MPIEXEC} -n 4 ./citcomcu ./tests/parallel2 -time_output 0 -use_global_solution > parallel2_0_global.tmp 2>&1; \
      ${DIFF} tests/output/parallel2_0_global.out parallel2_0_global.tmp || echo ${PWD} "\nPossible problem with with parallel2_0_global, diffs above \n======================================"; \
      ${RM} parallel2_0_global.tmp

test_parallel2_0_petsc:
	-@${MPIEXEC} -n 4 ./citcomcu ./tests/parallel2 -time_output 0 -use_petsc_linear_solver > parallel2_0_global.tmp 2>&1; \
      ${DIFF} tests/output/parallel2_0_global.out parallel2_0_global.tmp || echo ${PWD} "\nPossible problem with with parallel2_0_petsc, diffs above \n======================================"; \
      ${RM} parallel2_0_global.tmp

test_parallel2_1:
	-@${MPIEXEC} -n 4 ./citcomcu ./tests/parallel2 -time_limit 1.0e-3 -time_output 0 > parallel2_1.tmp 2>&1; \
      ${DIFF} tests/output/parallel2_1.out parallel2_1.tmp || echo ${PWD} "\nPossible problem with with parallel2_1, diffs above \n======================================"; \
      ${RM} parallel2_1.tmp

test_parallel3_0:
	-@${MPIEXEC} -n 4 ./citcomcu ./tests/parallel3 -time_output 0 > parallel3_0.tmp 2>&1; \
      ${DIFF} tests/output/parallel3_0.out parallel3_0.tmp || echo ${PWD} "\nPossible problem with with parallel3_0, diffs above \n======================================"; \
      ${RM} parallel3_0.tmp

test_parallel3_0_global:
	-@${MPIEXEC} -n 4 ./citcomcu ./tests/parallel3 -time_output 0 -use_global_solution > parallel3_0_global.tmp 2>&1; \
      ${DIFF} tests/output/parallel3_0_global.out parallel3_0_global.tmp || echo ${PWD} "\nPossible problem with with parallel3_0_global, diffs above \n======================================"; \
      ${RM} parallel3_0_global.tmp

test_parallel3_0_petsc:
	-@${MPIEXEC} -n 4 ./citcomcu ./tests/parallel3 -time_output 0 -use_petsc_linear_solver > parallel3_0_global.tmp 2>&1; \
      ${DIFF} tests/output/parallel3_0_global.out parallel3_0_global.tmp || echo ${PWD} "\nPossible problem with with parallel3_0_petsc, diffs above \n======================================"; \
      ${RM} parallel3_0_global.tmp

test_parallel3_1:
	-@${MPIEXEC} -n 4 ./citcomcu ./tests/parallel3 -time_limit 1.0e-3 -time_output 0 > parallel3_1.tmp 2>&1; \
      ${DIFF} tests/output/parallel3_1.out parallel3_1.tmp || echo ${PWD} "\nPossible problem with with parallel3_1, diffs above \n======================================"; \
      ${RM} parallel3_1.tmp

test_serial1_serial1:
	./bin/viewOutput.py run_output/serial1 C run_output/serial1 P x

test_serial1_parallel1:
	./bin/viewOutput.py run_output/serial1 C run_output/parallel1 C x
	./bin/viewOutput.py run_output/serial1 C run_output/parallel1 C y
	./bin/viewOutput.py run_output/serial1 C run_output/parallel1 C z
	./bin/viewOutput.py run_output/serial1 C run_output/parallel1 P x
	./bin/viewOutput.py run_output/serial1 C run_output/parallel1 P y
	./bin/viewOutput.py run_output/serial1 C run_output/parallel1 P z

test_serial1_parallel2:
	./bin/viewOutput.py run_output/serial1 C run_output/parallel2 C x
	./bin/viewOutput.py run_output/serial1 C run_output/parallel2 C y
	./bin/viewOutput.py run_output/serial1 C run_output/parallel2 C z
	./bin/viewOutput.py run_output/serial1 C run_output/parallel2 P x
	./bin/viewOutput.py run_output/serial1 C run_output/parallel2 P y
	./bin/viewOutput.py run_output/serial1 C run_output/parallel2 P z

test_serial1_parallel3:
	./bin/viewOutput.py run_output/serial1 C run_output/parallel3 C x
	./bin/viewOutput.py run_output/serial1 C run_output/parallel3 C y
	./bin/viewOutput.py run_output/serial1 C run_output/parallel3 C z
	./bin/viewOutput.py run_output/serial1 C run_output/parallel3 P x
	./bin/viewOutput.py run_output/serial1 C run_output/parallel3 P y
	./bin/viewOutput.py run_output/serial1 C run_output/parallel3 P z

##############################################################################
#	PETSc information
##############################################################################

include ${PETSC_DIR}/conf/variables
include ${PETSC_DIR}/conf/rules
