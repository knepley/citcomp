/*
 * CitcomCU is a Finite Element Code that solves for thermochemical
 * convection within a three dimensional domain appropriate for convection
 * within the Earth's mantle. Cartesian and regional-spherical geometries
 * are implemented. See the file README contained with this distribution
 * for further details.
 * 
 * Copyright (C) 1994-2005 California Institute of Technology
 * Copyright (C) 2000-2005 The University of Colorado
 *
 * Authors: Louis Moresi, Shijie Zhong, and Michael Gurnis
 *
 * For questions or comments regarding this software, you may contact
 *
 *     Luis Armendariz <luis@geodynamics.org>
 *     http://geodynamics.org
 *     Computational Infrastructure for Geodynamics (CIG)
 *     California Institute of Technology
 *     2750 East Washington Blvd, Suite 210
 *     Pasadena, CA 91007
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 2 of the License, or any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <petsc.h>
#include "element_definitions.h"
#include "global_defs.h"

#undef __FUNCT__
#define __FUNCT__ "general_stokes_solver"
PetscErrorCode general_stokes_solver(struct All_variables *E, Vec oldU)
{
  DM             dmF = E->lmesh.levelFullDA[E->mesh.levmax];
  DM             dmV = E->lmesh.levelVeloDA[E->mesh.levmax];
  DM             dmP = E->lmesh.levelPresDA[E->mesh.levmax];
  VecScatter     scV = E->lmesh.veloMap;
  VecScatter     scP = E->lmesh.presMap;
  SNES           snes;
  KSP            ksp;
  PC             pc;
  Mat            A;
  Vec            x, b, deltaUVec, globalUVec, globalPVec;
  PetscScalar   *deltaU;
  PetscReal      Udot_mag = 0.0, dUdot_mag = 0.0;
  double         time;
  double         timestartLoop, timethisit; /* MAJ 10/28/008*/
  double         timeLimit = E->monitor.time_limit*3600;
  PetscBool      useGlobal = E->control.useGlobalSolution;
  PetscBool      usePetscLinear    = E->control.usePetscLinearSolver;
  PetscBool      usePetscNonlinear = E->control.usePetscNonlinearSolver;
  PetscInt       count;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  dUdot_mag = 0.0;

  ierr = DMGetLocalVector(dmV, &deltaUVec);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dmV, &globalUVec);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dmP, &globalPVec);CHKERRQ(ierr);
  ierr = VecGetArray(deltaUVec, &deltaU);CHKERRQ(ierr); --deltaU;

  E->monitor.elapsed_time_vsoln1 = E->monitor.elapsed_time_vsoln;
  E->monitor.elapsed_time_vsoln  = E->monitor.elapsed_time;

  if (E->parallel.me == 0) time = CPU_time0();

  ierr = VecGetArray(E->UVec, &E->U);CHKERRQ(ierr); --E->U;
  velocities_conform_bcs(E, E->U);
  ++E->U; ierr = VecRestoreArray(E->UVec, &E->U);CHKERRQ(ierr);

  ierr = VecGetArray(E->FVec, &E->F);CHKERRQ(ierr); --E->F;
  assemble_forces(E, E->F, 0);
  ++E->F; ierr = VecRestoreArray(E->FVec, &E->F);CHKERRQ(ierr);

	count = 1;

	do
	{
		timestartLoop = CPU_time0(); /*MAJ 10/28/08*/

		if(E->viscosity.update_allowed)
			get_system_viscosity(E, 1, E->EVI[E->mesh.levmax], E->VI[E->mesh.levmax]);

		construct_stiffness_B_matrix(E);

        if (!usePetscLinear && !usePetscNonlinear) {
          ierr = CitcomResidual(E);CHKERRQ(ierr);
          ierr = solve_constrained_flow_iterative(E);CHKERRQ(ierr);

          if (useGlobal) {
            ierr = DMLocalToGlobalBegin(dmP, E->PVec, INSERT_VALUES, globalPVec);CHKERRQ(ierr);
            ierr = DMLocalToGlobalEnd(dmP, E->PVec, INSERT_VALUES, globalPVec);CHKERRQ(ierr);
            ierr = DMGlobalToLocalBegin(dmP, globalPVec, INSERT_VALUES, E->PVec);CHKERRQ(ierr);
            ierr = DMGlobalToLocalEnd(dmP, globalPVec, INSERT_VALUES, E->PVec);CHKERRQ(ierr);

            ierr = DMLocalToGlobalBegin(dmV, E->UVec, INSERT_VALUES, globalUVec);CHKERRQ(ierr);
            ierr = DMLocalToGlobalEnd(dmV, E->UVec, INSERT_VALUES, globalUVec);CHKERRQ(ierr);
            ierr = DMGlobalToLocalBegin(dmV, globalUVec, INSERT_VALUES, E->UVec);CHKERRQ(ierr);
            ierr = DMGlobalToLocalEnd(dmV, globalUVec, INSERT_VALUES, E->UVec);CHKERRQ(ierr);
          }
          ierr = CitcomResidual(E);CHKERRQ(ierr);
        } else if (usePetscLinear) {
          ierr = CitcomResidual(E);CHKERRQ(ierr);
          ierr = MatCreateShell(PetscObjectComm((PetscObject) dmF), 0, 0, PETSC_DETERMINE, PETSC_DETERMINE, E, &A);CHKERRQ(ierr);
          ierr = MatSetUp(A);CHKERRQ(ierr);

          ierr = PCCreate(PetscObjectComm((PetscObject) dmF), &pc);CHKERRQ(ierr);
          ierr = PCSetType(pc, PCSHELL);CHKERRQ(ierr);
          ierr = PCShellSetContext(pc, E);CHKERRQ(ierr);
          ierr = PCShellSetApply(pc, CitcomLinearSolver);CHKERRQ(ierr);

          ierr = KSPCreate(PetscObjectComm((PetscObject) dmF), &ksp);CHKERRQ(ierr);
          ierr = KSPSetType(ksp, KSPPREONLY);CHKERRQ(ierr);
          ierr = KSPSetPC(ksp, pc);CHKERRQ(ierr);
          ierr = KSPSetOperators(ksp, A, A, DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);

          ierr = DMGetGlobalVector(dmF, &b);CHKERRQ(ierr);
          ierr = DMGetGlobalVector(dmF, &x);CHKERRQ(ierr);
          /* Construct b */
          ierr = VecSet(b, 0.0);
          ierr = DMLocalToGlobalBegin(dmV, E->FVec, INSERT_VALUES, globalUVec);CHKERRQ(ierr);
          ierr = DMLocalToGlobalEnd(dmV, E->FVec, INSERT_VALUES, globalUVec);CHKERRQ(ierr);
          ierr = VecScatterBegin(scV, globalUVec, b, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
          ierr = VecScatterEnd(scV, globalUVec, b, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
#if 0
          /* Construct x */
          ierr = DMLocalToGlobalBegin(dmP, E->PVec, INSERT_VALUES, globalPVec);CHKERRQ(ierr);
          ierr = DMLocalToGlobalEnd(dmP, E->PVec, INSERT_VALUES, globalPVec);CHKERRQ(ierr);
          ierr = VecScatterBegin(scP, globalPVec, x, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
          ierr = VecScatterEnd(scP, globalPVec, x, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
          ierr = DMLocalToGlobalBegin(dmV, E->UVec, INSERT_VALUES, globalUVec);CHKERRQ(ierr);
          ierr = DMLocalToGlobalEnd(dmV, E->UVec, INSERT_VALUES, globalUVec);CHKERRQ(ierr);
          ierr = VecScatterBegin(scV, globalUVec, x, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
          ierr = VecScatterEnd(scV, globalUVec, x, INSERT_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
#endif
          /* Solve */
          ierr = KSPSolve(ksp, b, x);CHKERRQ(ierr);
          /* Read out solution */
          ierr = VecScatterBegin(scV, x, globalUVec, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
          ierr = VecScatterEnd(scV, x, globalUVec, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
          ierr = VecScatterBegin(scP, x, globalPVec, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
          ierr = VecScatterEnd(scP, x, globalPVec, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
          ierr = DMRestoreGlobalVector(dmF, &b);CHKERRQ(ierr);
          ierr = DMRestoreGlobalVector(dmF, &x);CHKERRQ(ierr);
          ierr = DMGlobalToLocalBegin(dmV, globalUVec, INSERT_VALUES, E->UVec);CHKERRQ(ierr);
          ierr = DMGlobalToLocalEnd(dmV, globalUVec, INSERT_VALUES, E->UVec);CHKERRQ(ierr);
          ierr = DMGlobalToLocalBegin(dmP, globalPVec, INSERT_VALUES, E->PVec);CHKERRQ(ierr);
          ierr = DMGlobalToLocalEnd(dmP, globalPVec, INSERT_VALUES, E->PVec);CHKERRQ(ierr);
          /* Cleanup */
          ierr = MatDestroy(&A);CHKERRQ(ierr);
          ierr = PCDestroy(&pc);CHKERRQ(ierr);
          ierr = KSPDestroy(&ksp);CHKERRQ(ierr);

          ierr = VecGetArray(E->UVec, &E->U);CHKERRQ(ierr); --E->U;
          v_from_vector(E, E->V, E->U);
          ++E->U; ierr = VecRestoreArray(E->UVec, &E->U);CHKERRQ(ierr);
        } else {
          ierr = SNESCreate(PetscObjectComm((PetscObject) dmF), &snes);CHKERRQ(ierr);
          ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
          /* Solve */
          ierr = DMGetGlobalVector(dmF, &x);CHKERRQ(ierr);
          ierr = SNESSolve(snes, NULL, x);CHKERRQ(ierr);
          /* Read out solution */
          ierr = VecScatterBegin(scV, x, globalUVec, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
          ierr = VecScatterEnd(scV, x, globalUVec, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
          ierr = VecScatterBegin(scP, x, globalPVec, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
          ierr = VecScatterEnd(scP, x, globalPVec, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
          ierr = DMRestoreGlobalVector(dmF, &x);CHKERRQ(ierr);
          ierr = DMGlobalToLocalBegin(dmV, globalUVec, INSERT_VALUES, E->UVec);CHKERRQ(ierr);
          ierr = DMGlobalToLocalEnd(dmV, globalUVec, INSERT_VALUES, E->UVec);CHKERRQ(ierr);
          ierr = DMGlobalToLocalBegin(dmP, globalPVec, INSERT_VALUES, E->PVec);CHKERRQ(ierr);
          ierr = DMGlobalToLocalEnd(dmP, globalPVec, INSERT_VALUES, E->PVec);CHKERRQ(ierr);
          /* Cleanup */
          ierr = SNESDestroy(&snes);CHKERRQ(ierr);

          ierr = VecGetArray(E->UVec, &E->U);CHKERRQ(ierr); --E->U;
          v_from_vector(E, E->V, E->U);
          ++E->U; ierr = VecRestoreArray(E->UVec, &E->U);CHKERRQ(ierr);
        }

		if(E->viscosity.SDEPV || E->viscosity.TSCDEPV) /* MIB, JAN07 added TSCDEPV */
		{
            ierr = VecWAXPY(deltaUVec, -1.0, oldU, E->UVec);CHKERRQ(ierr);
            ierr = VecCopy(E->UVec, oldU);CHKERRQ(ierr);
            ierr = DMLocalToGlobalBegin(dmV, E->UVec, INSERT_VALUES, globalUVec);CHKERRQ(ierr);
            ierr = DMLocalToGlobalEnd(dmV, E->UVec, INSERT_VALUES, globalUVec);CHKERRQ(ierr);
            ierr = VecNorm(globalUVec, NORM_2, &Udot_mag);CHKERRQ(ierr);
            ierr = DMLocalToGlobalBegin(dmV, deltaUVec, INSERT_VALUES, globalUVec);CHKERRQ(ierr);
            ierr = DMLocalToGlobalEnd(dmV, deltaUVec, INSERT_VALUES, globalUVec);CHKERRQ(ierr);
            ierr = VecNorm(globalUVec, NORM_2, &dUdot_mag);CHKERRQ(ierr);

			if(Udot_mag != 0.0)
				dUdot_mag /= Udot_mag;

			//	if(E->control.sdepv_print_convergence < E->monitor.solution_cycles && E->parallel.me == 0)
			if(E->control.sdepv_print_convergence && E->parallel.me == 0)
			{
				fprintf(stderr, "Stress dependent viscosity: DUdot = %.4e (%.4e) for iteration %d\n", dUdot_mag, Udot_mag, count);
				fprintf(E->fp, "Stress dependent viscosity: DUdot = %.4e (%.4e) for iteration %d\n", dUdot_mag, Udot_mag, count);
				fflush(E->fp);
			}
			/* Output newtonian solution for zeroeth time-step only */
			if (E->control.output_sdepvnewt_stepzero && (E->monitor.solution_cycles == 0) && (count == 1)) {
				if (E->parallel.me == 0)
					fprintf(stderr,"Output newtonian solution for step zero\n");
				process_temp_field(E,E->monitor.solution_cycles-1);
				process_new_velocity(E,E->monitor.solution_cycles-1);
			}
			count++;
			timethisit = CPU_time0() - timestartLoop; /*MAJ 10/28/08*/
            if (E->monitor.time_output) {
              if (E->parallel.me == 0)
                fprintf(stderr, "count = %d. CPUtime = %0.4e. (%0.4es) timestartLoop = %0.4e, timethisit = %0.4e (%0.4e). timeLimit = %0.4e. timeLimit-timethisit = %0.4e. \n", count, (CPU_time0()/3600), CPU_time0() - E->monitor.initial_time, (timestartLoop/3600), (timethisit/3600), ((CPU_time0() - timestartLoop)/3600), (timeLimit/3600), ((timeLimit-timethisit)/3600));
            } else {
              ierr = PetscPrintf(PETSC_COMM_WORLD, "count = %d.\n", count);CHKERRQ(ierr);
              /* ierr = PetscPrintf(PETSC_COMM_WORLD, "||U - U_old||/||U|| = %g > %g, ||U|| = %g\n", dUdot_mag, E->viscosity.sdepv_misfit, Udot_mag);CHKERRQ(ierr); */
            }
		}						/* end for SDEPV   */

	} while((count < 50) && ((CPU_time0() - E->monitor.initial_time) < (timeLimit - timethisit)) && (dUdot_mag > E->viscosity.sdepv_misfit) && (E->viscosity.SDEPV || E->viscosity.TSCDEPV)); /* MAJ 10/28/08 timeLimit part. MIB, JAN07 added TSCDEPV */

  ++deltaU; ierr = VecRestoreArray(deltaUVec, &deltaU);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dmP, &globalPVec);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dmV, &globalUVec);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dmV, &deltaUVec);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
